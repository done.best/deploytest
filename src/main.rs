extern crate chrono;
use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use chrono::{Local, Utc};

fn done_build_version_info() -> &'static str {
    option_env!("DONE_CI_VERSION")
        .unwrap_or("Environment variable DONE_CI_VERSION was not set on build")
}

fn done_build_ts() -> &'static str {
    option_env!("DONE_CI_BUILDTS").unwrap_or("NO BUILD TS")
}

fn done_build_info() -> String {
    format!("{} [{}]", done_build_version_info(), done_build_ts())
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body(format!(
        "Hello--World! at UTC {} Local {} with version ({:?})",
        Utc::now(),
        Local::now(),
        done_build_info()
    ))
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body(format!(
        "Manual >> Hello World! at UTC {} Local {} with version ({:?})",
        Utc::now(),
        Local::now(),
        done_build_info()
    ))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!(
        "Actix-Web: Hello serving on 8080, started at UTC {} Local {} with version [{:?}]",
        Utc::now(),
        Local::now(),
        done_build_info()
    );

    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
            .route("/hey", web::get().to(manual_hello))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
