# Dockerfile for creating a statically-linked Rust application using docker's
# multi-stage build feature. This also leverages the docker build cache to avoid
# re-downloading dependencies if they have not changed.
#FROM rust:1.49.0 AS build
FROM rust:1.50-alpine3.13 as build
WORKDIR /usr/src

#Define built time environment variable, set with --build-arg
ARG DONE_CI_VERSION 
ARG DONE_CI_BUILDTS

# Download the target for static linking.
RUN apk add --no-cache musl-dev && rustup component add rustfmt clippy && rustup target add x86_64-unknown-linux-musl

# Create a dummy project and build the app's dependencies.
# If the Cargo.toml or Cargo.lock files have not changed,
# we can use the docker build cache and skip these (typically slow) steps.
RUN USER=root cargo new dummy
WORKDIR /usr/src/dummy
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

# Copy the source and build the application.
COPY src ./src
RUN cargo  fmt -- --check || (echo "*** FAIL > Rust code format check failed" 1>&2 && exit 99)
RUN cargo clippy --workspace -- -D warnings || (echo "*** FAIL > Rust clippy linting failed" 1>&2 && exit 101)
RUN cargo install --target x86_64-unknown-linux-musl --path . 
RUN strip /usr/local/cargo/bin/deploytest

# Copy the statically-linked binary into a scratch container.
FROM scratch
COPY --from=build /usr/local/cargo/bin/deploytest .
USER 1000
#CMD ["./deploytest"]
ENTRYPOINT ["./deploytest"]