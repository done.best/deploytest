#!/bin/sh
export DONE_CI_VERSION=$(git describe --always --tags --long --dirty)
export DONE_CI_BUILDTS=$(date --iso-8601=seconds -u)
echo "Building Version [$DONE_CI_VERSION] IMAGE  [$DONE_CI_IMAGE_TAG] BUILD TS [$DONE_CI_BUILDTS]"
docker build -f ci-src/dockerfiles/rust-build-shell.dockerfile -t $DONE_CI_IMAGE_TAG --build-arg DONE_CI_VERSION=${DONE_CI_VERSION} --build-arg DONE_CI_BUILDTS=${DONE_CI_BUILDTS} .