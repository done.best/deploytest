#!/bin/sh
echo "Cleaning up k8s ressources"
kubectl delete -n done-staging deployment deploytest
kubectl delete -n done-staging service deploytest-stage-service
kubectl delete -n done-staging ingress deploytest-stage-ingress
echo "Creating k8s ressources"
kubectl apply -f ci-src/k8s/deploytest.k8sdeployment.yaml
kubectl apply -f ci-src/k8s/deploytest.k8sservice.yaml
kubectl apply -f ci-src/k8s/deploytest.k8singress.yaml
